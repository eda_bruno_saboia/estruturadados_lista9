package br.ufc.list9

class Node {
  var value: Int = 0
  var parent: Node = null
  var nodeR: Node = null
  var nodeL: Node = null

  override def toString(): String = {
    return "Node [valor=" + value + "]"
  }

}