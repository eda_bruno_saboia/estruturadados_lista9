package br.ufc.list9

object Main {
  
  def main(args: Array[String]) { 
    var abb:Abb = new Abb
    abb.insert(5)
    abb.insert(4)
    abb.insert(6)
    abb.insert(7)
    abb.insert(3)
    abb.insert(2)
    abb.insert(10)
    println("Valor encontrado: " + abb.findValue(3))
    
    abb.printTree()
    abb.remove(3)
    abb.printTree()
    abb.findValue(3)
    
    println("Maior Valor: " + abb.encontraMaximo())
    println("O pai é: " + abb.findValueParent(4))
        
  }
}