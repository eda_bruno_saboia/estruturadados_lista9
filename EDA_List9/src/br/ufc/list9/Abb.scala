package br.ufc.list9

class Abb {
  var root: Node = new Node

  def isEmpty(): Boolean = {
    if (root == null) {
      return true;
    }
    return false;
  }
      
  def getAltura(): Int={
        return getAltura(this.root)
    }
    
  private def getAltura(root: Node): Int={
        if(root == null){
            return 0;
        }
        var altL = getAltura(root.nodeL)
        var altR = getAltura(root.nodeR)
        if(altL > altR){
            return altL + 1
        } else {
            return altR + 1
        }
  }
  
  def getQtdNode(): Int={
        return getQtdNode(root)
  }
    
  private def getQtdNode(root: Node): Int={
        if(root == null){
            return 0;
        }
        var qtdNodeL = getQtdNode(root.nodeL)
        var qtdNodeR = getQtdNode(root.nodeR)
        return qtdNodeL + qtdNodeR + 1
  }
    
  def printTree(){
        if(root == null)
            println("Árvore vazia");
        else
            printTree(root);
  }
    
  private def printTree(node: Node){
        if(node.nodeL != null){
            printTree(node.nodeL)
        }
        if (node.nodeR != null){
            printTree(node.nodeR)
        }
        println("Nó: " + node.value)
  }
    
  def insert(value: Int){
        insert(this.root, value)
  }
    
  private def insert(node: Node, value: Int): Unit ={
    var newnode: Node  = new Node    
    if(this.root.value == 0){
      newnode.value = value
      root = newnode
    } else {
            if (value < node.value) {
                if (node.nodeL != null) { 
                    insert(node.nodeL, value) 
                } else { 
                    //Se no esquerdo vazio insere o novo no aqui 
                  newnode.value = value  
                  node.nodeL = newnode  
                } 
                //Verifica se o valor a ser inserido é maior que o no corrente da árvore, se sim vai para subarvore direita 
            } else if (value > node.value) { 
                //Se tiver elemento no no direito continua a busca 
                if (node.nodeR != null) { 
                    insert(node.nodeR, value) 
                } else {
                    //Se nodo direito vazio insere o novo no aqui 
                  newnode.value = value  
                  node.nodeR = newnode  
                  
                } 
            }
        }
    }
   
  def remove(value: Int): Node={
        return remove(root, value)
  }
    
  private def remove(node: Node, value: Int): Node={
        if(this.root == null){
            println("Árvore vazia")
            return node
        } else {            
            if(value < node.value){
                node.nodeL_=(remove(node.nodeL, value))
            } else if(value > node.value){
                node.nodeR_=(remove(node.nodeR, value))
            } else if (node.nodeL != null && node.nodeR != null) {
                /*2 filhos*/  
                println("  Removeu No " + node.value)
                node.value = encontraMinimo(node.nodeR).value
                node.nodeR = removeMinimo(node.nodeR)
            } else {  
                println("  Removeu No " + node.value);  
                if (node.nodeL != null){
                  var changeNode = node.nodeL
                  
                  return changeNode
                  
                }else{
                  var changeNode = node.nodeR
                  
                  return changeNode
                }
            }  
            return node;
        }
  }
    
  def removeMinimo(node: Node): Node ={  
        if (node == null) {  
            println("  ERRO ")  
        } else if (node.nodeL != null) {  
            node.nodeL =(removeMinimo(node.nodeL));  
            return node;  
        } else {  
            return node.nodeR;  
        }  
        return null;  
  }  
  
  private def encontraMinimo(node: Node): Node ={  
    var newNode = new Node
    newNode = node
    if (node != null) {  
            while (newNode.nodeL != null) {  
                newNode = newNode.nodeL  
            }  
        }  
        return newNode  
  }
  
  def encontraMaximo(): Node={
        return encontraMaximo(root)
  }
  
  private def encontraMaximo(node: Node): Node ={  
    var newNode = new Node
    newNode = node
    if (node != null) {  
            while (newNode.nodeR != null) {  
                newNode = newNode.nodeR  
            }  
        }  
        return newNode  
  }
  
  def findValue(value: Int): Node={
        return findValue(root, value)
  }
  
  private def findValue(node: Node, value: Int): Node={
        if(node == null){
          println("Valor não encontrado")  
          return null
        } else {
            if (value == node.value){
              return node
            } else if(value < node.value){
                findValue(node.nodeL, value)
            } else{
                findValue(node.nodeR, value)
            } 
            
        }
  }
  
  def findValueParent(value: Int): Node={
        return findValueParent(root,null ,value)
  }
  
  private def findValueParent(nodeActual: Node, nodeParent: Node,  value: Int): Node={
        if(nodeActual == null){
          println("Valor não encontrado")  
          return null
        } else {
            if (value == nodeActual.value){
              return nodeParent
            } else if(value < nodeActual.value){
                var parent = nodeActual
                findValueParent(nodeActual.nodeL, parent, value)
            } else{
                var parent = nodeActual
                findValueParent(nodeActual.nodeR,parent, value)
            } 
            
        }
  }
  
  
}
